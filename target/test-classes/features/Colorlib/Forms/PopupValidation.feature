#Author: Estephanie Miranda
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Verificar el funcinamiento de la pantalla POPUP Validation
  Como usuario de ColorLib
  Quiero que se muestre el popup de validacion
  para validar el funcionamiento

  @CasoExitoso
  Scenario: Diligenciamiento exitoso del formulario pop validation,
  					no se presenta ningún mensaje de validación.
  					
    Given se autentica el usuario en ColorLib con usr "demo" y pass "demo"
    And Ingresa a Forms validations
    When Se llenan todos los campos
    	| required | select | multipleS1 | url1                  | email           | pass1  | pass2  | minSize | maxSize | number | ip          | date       | dateEarlier |
        | Valor1   | Golf   | Tennis     | http://www.valor1.com | valor1@mail.com | valor1 | valor1 | 123456  | 123456  | -99.99 | 200.200.5.4 | 2021-01-22 | 2012/09/12  |
    Then Se verifique el registro exitoso

  @CasoAlterno
  Scenario: Diligenciamiento con errores del formulario pop validation,
  					se presenta globo informativo indicando error en el diligenciamiento de algunos de los campos.
  					
    Given se autentica el usuario en ColorLib con usr "demo" y pass "demo"
    And Ingresa a Forms validations
    When Se llenan todos los campos
        | required | select | multipleS1 | url1                  | email           | pass1  | pass2  | minSize | maxSize | number | ip          | date       | dateEarlier |
        #|          | Golf          | Tennis     | http://www.valor1.com | valor1@mail.com | valor1 | valor1 | 123456  | 123456  | -99.99 | 200.200.5.4 | 2021-01-22 | 2012/09/12  |
        | Valor1   | Choose a sport| Tennis     | http://www.valor1.com | valor1@mail.com | valor1 | valor1 | 123456  | 123456  | -99.99 | 200.200.5.4 | 2021-01-22 | 2012/09/12  |
    Then Verificar que se presente Globo Informativo de validación

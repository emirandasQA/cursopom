package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*; 

public class ColorlibFormValidationPage extends PageObject{
//Campo required
	@FindBy(id="req")
	public WebElementFacade txtRequired;
//Campo Seleccion de deporte
	@FindBy(id="sport")
	public WebElementFacade cmbSport1;
//Campo Multiple seleccion	
	@FindBy(id="sport2")
	public WebElementFacade cmbSport2;
//Campo url
	@FindBy(id="url1")
	public WebElementFacade txtUrl;
//Campo email
	@FindBy(id="email1")
	public WebElementFacade txtEmail1;
//Campo Password1
	@FindBy(id="pass1")
	public WebElementFacade txtPass1;
//Campo Password2
	@FindBy(id="pass2")
	public WebElementFacade txtPass2;
//Campo MinSize
	@FindBy(id="minsize1")
	public WebElementFacade txtMinSize1;
//Campo MaxSize
	@FindBy(id="maxsize1")
	public WebElementFacade txtMaxSize1;
//Campo Number
	@FindBy(id="number2")
	public WebElementFacade txtNumber;
//Campo IP
	@FindBy(id="ip")
	public WebElementFacade txtIp;
//Campo Date
	@FindBy(id="date3")
	public WebElementFacade txtDate;
//Campo Date Earlier
	@FindBy(id="past")
	public WebElementFacade txtDateEarlier;
//Boton Validate	
	@FindBy(xpath="(//INPUT[@type='submit'])[1]")
	public WebElementFacade btnValidate;
//Mensaje informativo	
	@FindBy(xpath="(//DIV[@class='formErrorContent'])[1]")
	public WebElementFacade globoInformativo;
	
	public void Required(String datoPrueba) {
		txtRequired.click();
		txtRequired.clear();
		txtRequired.sendKeys(datoPrueba);
	}
	public void Select_Sport(String datoPrueba) {
		cmbSport1.click();
		cmbSport1.selectByVisibleText(datoPrueba);
	}
	public void Multiple_Select(String datoPrueba) {
		cmbSport2.click();
		cmbSport2.selectByVisibleText(datoPrueba);
	}
	public void url(String datoPrueba) {
		txtUrl.click();
		txtUrl.clear();
		txtUrl.sendKeys(datoPrueba);
	}
	public void email(String datoPrueba) {
		txtEmail1.click();
		txtEmail1.clear();
		txtEmail1.sendKeys(datoPrueba);
	}
	public void password(String datoPrueba) {
		txtPass1.click();
		txtPass1.clear();
		txtPass1.sendKeys(datoPrueba);
	}
	public void confirm_password(String datoPrueba) {
		txtPass2.click();
		txtPass2.clear();
		txtPass2.sendKeys(datoPrueba);
	}
	public void minimun_field_size(String datoPrueba) {
		txtMinSize1.click();
		txtMinSize1.clear();
		txtMinSize1.sendKeys(datoPrueba);
	}
	public void maximun_field_size(String datoPrueba) {
		txtMaxSize1.click();
		txtMaxSize1.clear();
		txtMaxSize1.sendKeys(datoPrueba);
	}
	public void number(String datoPrueba) {
		txtNumber.click();
		txtNumber.clear();
		txtNumber.sendKeys(datoPrueba);
	}
	public void ip(String datoPrueba) {
		txtIp.click();
		txtIp.clear();
		txtIp.sendKeys(datoPrueba);
	}
	public void date(String datoPrueba) {
		txtDate.click();
		txtDate.clear();
		txtDate.sendKeys(datoPrueba);
	}
	public void date_earlier(String datoPrueba) {
		txtDateEarlier.click();
		txtDateEarlier.clear();
		txtDateEarlier.sendKeys(datoPrueba);
	}
	public void validate() {
		btnValidate.click();
		btnValidate.sendKeys();
	}
	public void form_sin_errores() {
		assertThat(globoInformativo.isCurrentlyVisible(), is(false));
	}
	public void form_con_errores() {
		assertThat(globoInformativo.isCurrentlyVisible(), is(true));
	}

}

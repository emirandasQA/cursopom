package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.model.RegistroData;
import com.choucair.formacion.pageobjects.ColorlibFormValidationPage;
import net.thucydides.core.annotations.Step;

public class ColorlibFormValidationSteps {
	
	ColorlibFormValidationPage colorlibFormValidationPage;
	
	@Step
	public void diligenciar_popup_datos_tabla(List<RegistroData> registroData) {
		colorlibFormValidationPage.Required(registroData.get(0).getRequired().trim());
		colorlibFormValidationPage.Select_Sport(registroData.get(0).getSelect().trim());
		colorlibFormValidationPage.Multiple_Select(registroData.get(0).getMultipleS1().trim());
		colorlibFormValidationPage.url(registroData.get(0).getUrl1().trim());
		colorlibFormValidationPage.email(registroData.get(0).getEmail().trim());
		colorlibFormValidationPage.password(registroData.get(0).getPass1().trim());
		colorlibFormValidationPage.confirm_password(registroData.get(0).getPass2().trim());
		colorlibFormValidationPage.minimun_field_size(registroData.get(0).getMinSize().trim());
		colorlibFormValidationPage.maximun_field_size(registroData.get(0).getMaxSize().trim());
		colorlibFormValidationPage.number(registroData.get(0).getNumber().trim());
		colorlibFormValidationPage.ip(registroData.get(0).getIp().trim());
		colorlibFormValidationPage.date(registroData.get(0).getDate().trim());
		colorlibFormValidationPage.date_earlier(registroData.get(0).getDateEarlier().trim());
		colorlibFormValidationPage.validate();
	}
	
	@Step
	public void verificar_ingreso_datos_formulario_exitoso() {
		colorlibFormValidationPage.form_sin_errores();
	}
	@Step
	public void verificar_ingreso_datos_formulario_con_errores() {
		colorlibFormValidationPage.form_con_errores();
	}

}

package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.PopupValidationSteps;
import com.choucair.formacion.model.RegistroData;
import com.choucair.formacion.steps.ColorlibFormValidationSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PopupValidatonDefinition {
	@Steps
	PopupValidationSteps popupValidationSteps;
	
	@Steps
	ColorlibFormValidationSteps colorlibFormValidatioSteps;
	
	@Given("^se autentica el usuario en ColorLib con usr \"([^\"]*)\" y pass \"([^\"]*)\"$")
	public void se_autentica_el_usuario_en_ColorLib_con_usr_y_pass(String Usuario, String Clave) {
		popupValidationSteps.login_colorlib(Usuario, Clave);
	}

	@Given("^Ingresa a Forms validations$")
	public void ingresa_a_Forms_validations() {
	   popupValidationSteps.ingresar_form_validation();
	}

	@When("^Se llenan todos los campos$")
	public void se_llenan_todos_los_campos(List<RegistroData> registroData) {
		colorlibFormValidatioSteps.diligenciar_popup_datos_tabla(registroData);
	}

	@Then("^Se verifique el registro exitoso$")
	public void se_verifique_el_registro_exitoso() {
		colorlibFormValidatioSteps.verificar_ingreso_datos_formulario_exitoso();
	}
	
	@Then("^Verificar que se presente Globo Informativo de validación$")
	public void verificar_que_se_presente_Globo_Informativo_de_validación() {
		colorlibFormValidatioSteps.verificar_ingreso_datos_formulario_con_errores();
	}

}

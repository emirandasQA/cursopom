package com.choucair.formacion.model;

public class RegistroData {
	private String required;
	private String select;
	private String multipleS1;
	private String url1;
	private String email;
	private String pass1;
	private String pass2;
	private String minSize;
	private String maxSize;
	private String number;
	private String ip;
	private String date;
	private String dateEarlier;
	
	public String getRequired() { return required; }
	public void setRequired(String required) { this.required = required; }
	
	public String getSelect() { return select; }
	public void setSelect(String select) { this.select = select; }
	
	public String getMultipleS1() { return multipleS1; }
	public void setMultipleS1(String multipleS1) { this.multipleS1 = multipleS1; }
	
	public String getUrl1() { return url1; }
	public void setUrl1(String url1) { this.url1 = url1; }
	
	public String getEmail() { return email; }
	public void setEmail(String email) { this.email = email; }
	
	public String getPass1() { return pass1; }
	public void setPass1(String pass1) { this.pass1 = pass1; }
	
	public String getPass2() { return pass2; }
	public void setPass2(String pass2) { this.pass2 = pass2; }
	
	public String getMinSize() { return minSize; }
	public void setMinSize(String minSize) { this.minSize = minSize; }
	
	public String getMaxSize() { return maxSize; }
	public void setMaxSize(String maxSize) { this.maxSize = maxSize; }
	
	public String getNumber() { return number; }
	public void setNumber(String number) { this.number = number; }
	
	public String getIp() { return ip; }
	public void setIp(String ip) { this.ip = ip; }
	
	public String getDate() { return date; }
	public void setDate(String date) { this.date = date; }
	
	public String getDateEarlier() { return dateEarlier; }
	public void setDateEarlier(String dateEarlier) { this.dateEarlier = dateEarlier; }
	
}
